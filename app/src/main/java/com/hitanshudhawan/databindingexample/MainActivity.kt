package com.hitanshudhawan.databindingexample

import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.hitanshudhawan.databindingexample.databinding.ActivityMainBinding

// https://youtu.be/T-nQP9fidKU
// https://developer.android.com/topic/libraries/data-binding/

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        DataBindingUtil.setContentView<ActivityMainBinding>(this, R.layout.activity_main)
            .apply {
                setLifecycleOwner(this@MainActivity)
                viewmodel = ViewModelProviders.of(this@MainActivity).get(MainViewModel::class.java)
            }
    }
}
