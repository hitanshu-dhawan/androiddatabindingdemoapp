package com.hitanshudhawan.databindingexample

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel

class MainViewModel : ViewModel() {

    val imageUrl = "http://hitanshudhawan.com/images/index/profile_pic.jpg"

    private val _randomNumber = MutableLiveData<String>().apply { value = "07101996" }
    val randomNumber: LiveData<String>
        get() = _randomNumber
    // we should not expose MutableLiveData because it's value can be changed from outside

    fun changeRandomNumber() {
        _randomNumber.value = (Math.random() * 10_00_00_000).toInt().toString()
    }

    val isChecked = MutableLiveData<Boolean>()

    fun switchStateChanged(isChecked: Boolean) {
        this.isChecked.value = isChecked
    }

}